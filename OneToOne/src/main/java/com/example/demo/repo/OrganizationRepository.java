package com.example.demo.repo;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.entity.*;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {

}
